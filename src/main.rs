#[macro_use]
extern crate serde_derive;
extern crate directories;
extern crate gdk_pixbuf;
extern crate gio;
extern crate gtk;
extern crate serde;
extern crate toml;
extern crate url;

mod chasopys;

use std::cell::RefCell;
use std::env::args;
use std::error;
use std::rc::Rc;

use gio::prelude::*;
use gtk::prelude::*;

use gtk::{AboutDialogExt, ButtonExt, DialogExt, GtkWindowExt};

use chasopys::preferences::{ApplicationPreferences, PreferencesDialog};
use chasopys::resources::{application_resources, load_and_register_application_resources};

struct ApplicationWindow {
    window:      gtk::ApplicationWindow,
    preferences: Rc<RefCell<ApplicationPreferences>>,
}

pub fn show_error_dialog<W, E>(parent: Option<&W>, error: &E)
where
    W: gtk::IsA<gtk::Window>,
    E: error::Error,
{
    let dialog = gtk::MessageDialog::new(
        parent,
        gtk::DialogFlags::MODAL,
        gtk::MessageType::Error,
        gtk::ButtonsType::Close,
        &format!("{}", error),
    );
    dialog.run();
}

impl ApplicationWindow {
    pub fn new(application: &gtk::Application) -> ApplicationWindow {
        // TODO: Let's simplify. We should be able to load UI. Release
        // version shouldn't have any errors related to GUI loading,
        // incorrect resource names, etc.
        let builder = gtk::Builder::new_from_resource(application_resources::MAIN_WINDOW);
        let window: gtk::ApplicationWindow = builder.get_object("main-window").unwrap();
        let about_button: gtk::Button = builder.get_object("about-button").unwrap();
        let preferences_button: gtk::Button = builder.get_object("preferences-button").unwrap();
        let _save_worklog_button: gtk::Button = builder.get_object("save-worklog-button").unwrap();

        // TODO: Handle any possible errors while reading preferences.
        let preferences = ApplicationPreferences::new().unwrap_or_default();
        let preferences = Rc::new(RefCell::new(preferences));

        window.set_application(application);
        ApplicationWindow::forbid_vertical_resizing(&window);

        let about_dialog = AboutDialog::new(&window);
        about_button.connect_clicked(move |_| about_dialog.show());

        {
            let window = window.clone();
            let preferences = preferences.clone();
            preferences_button.connect_clicked(move |_| {
                let preferences_dialog = PreferencesDialog::new(&window, preferences.clone());
                preferences_dialog.show();

                // TODO: Handle an error
                preferences.borrow_mut().save().unwrap();
            });
        }

        ApplicationWindow {
            window,
            preferences,
        }
    }

    pub fn show(&self) {
        self.window.show_all();
    }

    fn forbid_vertical_resizing(_window: &gtk::ApplicationWindow) {
        // TODO: gtk-rs has `GtkWindowExt` trait which doesn't have
        // `set_geometry_hints` method.
        //
        // let mut hints: gdk::Geometry;
        //
        // hints.max_height = -1;
        // hints.max_width  = gdk::Screen::width();
        //
        // window.set_geometry_hints(&hints, gdk::WindowHints::MAX_SIZE);
    }
}

struct AboutDialog {
    dialog: gtk::AboutDialog,
}

impl AboutDialog {
    pub fn new(parent: &gtk::ApplicationWindow) -> AboutDialog {
        let dialog = gtk::AboutDialog::new();

        dialog.set_transient_for(parent);

        let logo = gdk_pixbuf::Pixbuf::new_from_resource_at_scale(
            application_resources::APPLICATION_LOGO,
            128,
            128,
            true,
        ).expect("Failed to load application logo from resources.");

        let authors = env!("CARGO_PKG_AUTHORS").split(":").collect::<Vec<&str>>();
        dialog.set_authors(&authors);
        dialog.set_comments(env!("CARGO_PKG_DESCRIPTION"));
        dialog.set_license_type(gtk::License::MitX11);
        dialog.set_logo(&logo);
        dialog.set_program_name("Chasopys");
        dialog.set_version(env!("CARGO_PKG_VERSION"));
        dialog.set_website(env!("CARGO_PKG_HOMEPAGE"));
        dialog.set_website_label(env!("CARGO_PKG_HOMEPAGE"));

        AboutDialog { dialog }
    }

    pub fn show(&self) {
        self.dialog.run();
        self.dialog.hide();
    }
}

fn build_and_run_gui(application: &gtk::Application) {
    let application_window = ApplicationWindow::new(application);

    application_window.show();
}

fn main() {
    load_and_register_application_resources();

    let application = gtk::Application::new("com.manenko.chasopys", gio::ApplicationFlags::empty())
        .expect("Failed to create application");

    application.connect_activate(|application| build_and_run_gui(&application));
    application.run(&args().collect::<Vec<_>>());
}
