extern crate gio;
extern crate glib;

include!(concat!(env!("OUT_DIR"), "/glib_resource_bundle.rs"));

/// Loads and registers Glib resources for this application.
pub fn load_and_register_application_resources() {
    let bytes = glib::Bytes::from_static(RESOURCES_DATA);
    let resource = gio::Resource::new_from_data(&bytes).unwrap();
    gio::resources_register(&resource);
}

pub mod application_resources {
    pub const APPLICATION_LOGO: &str = "/com/manenko/chasopys/images/clock.png";
    pub const MAIN_WINDOW: &str = "/com/manenko/chasopys/ui/main_window.ui";
    pub const PREFERENCES_DIALOG: &str = "/com/manenko/chasopys/ui/preferences_dialog.ui";
}

pub mod application_info {
    pub const QUALIFIER: &str = "com";
    pub const ORGANISATION: &str = "Oleksandr Manenko";
    pub const NAME: &str = "Chasopys";
}
