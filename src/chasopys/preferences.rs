use directories::ProjectDirs;
use std::cell::RefCell;
use std::convert::From;
use std::error;
use std::fmt;
use std::fs;
use std::io;
use std::io::{Read, Write};
use std::path::Path;
use std::path::PathBuf;
use std::rc::Rc;
use std::result;

use gtk::{Builder,
          Dialog,
          DialogExt,
          Entry,
          EntryExt,
          GtkWindowExt,
          IsA,
          ListStore,
          TreeModelExt,
          TreeSelectionExt,
          TreeView,
          TreeViewExt,
          Window};
use toml;
use url::Url;

use chasopys::resources::{application_info, application_resources};

#[derive(Clone)]
pub struct PreferencesDialog {
    dialog:               Dialog,
    jira_api_token_entry: Entry,
    jira_email_entry:     Entry,
    jira_url_entry:       Entry,
    preferences:          Rc<RefCell<ApplicationPreferences>>,
}

impl PreferencesDialog {
    pub fn new<T>(parent: &T, preferences: Rc<RefCell<ApplicationPreferences>>) -> Self
    where
        T: IsA<Window>,
    {
        let builder = Builder::new_from_resource(application_resources::PREFERENCES_DIALOG);

        // It's safe to use unwrap here, because it may fail only if
        // there is something wrong in the .ui file and this is what
        // we can find during the first run.
        let ui = PreferencesDialog {
            dialog:               builder.get_object("preferences-dialog").unwrap(),
            jira_api_token_entry: builder.get_object("jira-api-token-entry").unwrap(),
            jira_email_entry:     builder.get_object("jira-email-entry").unwrap(),
            jira_url_entry:       builder.get_object("jira-url-entry").unwrap(),
            preferences:          preferences.clone(),
        };

        Self::select_first_category(&builder);

        ui.dialog.set_transient_for(parent);

        let preferences = &preferences.borrow().jira;

        if let Some(ref text) = preferences.api_token.as_ref() {
            ui.jira_api_token_entry.set_text(text);
        }

        if let Some(ref text) = preferences.email.as_ref() {
            ui.jira_email_entry.set_text(text);
        }

        if let Some(ref url) = preferences.url.as_ref() {
            ui.jira_url_entry.set_text(url.as_str());
        }

        ui
    }

    pub fn show(&self) {
        self.dialog.run();
        self.dialog.close();

        let mut preferences = self.preferences.borrow_mut();
        preferences.jira.email = self.jira_email_entry.get_text();
        preferences.jira.api_token = self.jira_api_token_entry.get_text();
        // TODO: Handle an error. Or think how to make an error impossible?
        preferences.jira.url = self
            .jira_url_entry
            .get_text()
            .map(|t| Url::parse(&t).unwrap());
    }

    fn select_first_category(builder: &Builder) {
        let tree_view: TreeView = builder
            .get_object("preferences-categories-tree-view")
            .unwrap();
        let list_store: ListStore = builder
            .get_object("preferences-categories-list-store")
            .unwrap();
        let first_item_iter = list_store.get_iter_first().unwrap();
        tree_view.get_selection().select_iter(&first_item_iter);
    }
}

mod serde_url {
    //------------------------------------------------------------------------------
    // Serde serialization of the url::Url
    //------------------------------------------------------------------------------

    use serde::{self, Deserialize, Deserializer, Serializer};
    use url::Url;

    pub fn serialize<S>(url: &Option<Url>, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match url {
            Some(url) => serializer.serialize_str(&url.to_string()),
            None => serializer.serialize_none(),
        }
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<Option<Url>, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s: Option<String> = Option::deserialize(deserializer)?;

        match s {
            Some(s) => Url::parse(&s)
                .map(|u| Some(u))
                .map_err(serde::de::Error::custom),
            None => Ok(None),
        }
    }

}

//------------------------------------------------------------------------------
// Error
//------------------------------------------------------------------------------

#[derive(Debug)]
pub enum Error {
    IO(io::Error),
    FormatError(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::IO(error) => error.fmt(f),
            Error::FormatError(description) => write!(f, "{}", description),
        }
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        match self {
            Error::IO(error) => error.description(),
            Error::FormatError(description) => &description,
        }
    }
}

impl From<io::Error> for Error {
    fn from(error: io::Error) -> Self {
        Error::IO(error)
    }
}

impl From<toml::de::Error> for Error {
    fn from(error: toml::de::Error) -> Self {
        Error::FormatError(format!("{}", error))
    }
}

impl From<toml::ser::Error> for Error {
    fn from(error: toml::ser::Error) -> Self {
        Error::FormatError(format!("{}", error))
    }
}

//------------------------------------------------------------------------------
// Result
//------------------------------------------------------------------------------

pub type Result<T> = result::Result<T, Error>;

//------------------------------------------------------------------------------
// Preferences
//------------------------------------------------------------------------------

#[derive(Clone, Debug, Serialize, Deserialize, Default)]
pub struct ApplicationPreferences {
    pub jira: JiraPreferences,
}

#[derive(Clone, Debug, Serialize, Deserialize, Default)]
pub struct JiraPreferences {
    #[serde(with = "self::serde_url")]
    pub url: Option<Url>,
    pub email: Option<String>,
    pub api_token: Option<String>,
}

impl ApplicationPreferences {
    pub fn new_from_reader(reader: &mut Read) -> Result<Self> {
        let mut buffer = String::new();
        reader.read_to_string(&mut buffer)?;

        let preferences = toml::from_str(&buffer).map_err(Error::from)?;

        Ok(preferences)
    }

    pub fn new() -> Result<Self> {
        let path = Self::path_to_preferences_file()?;

        if path.exists() {
            let file = fs::File::open(path)?;
            let mut reader = io::BufReader::new(file);

            Self::new_from_reader(&mut reader)
        } else {
            Ok(ApplicationPreferences {
                jira: JiraPreferences {
                    url:       None,
                    email:     None,
                    api_token: None,
                },
            })
        }
    }

    pub fn write(&self, writer: &mut Write) -> Result<()> {
        toml::to_string(&self)
            .map(|s| write!(writer, "{}", s))
            .and_then(|_| Ok(()))
            .map_err(Error::from)
    }

    pub fn save(&self) -> Result<()> {
        let path = Self::path_to_preferences_file()?;
        let file = fs::File::create(&path)?;
        let mut writer = io::BufWriter::new(file);

        self.write(&mut writer)
    }

    fn path_to_preferences_file() -> Result<PathBuf> {
        let config_dir = Self::create_and_get_config_dir()?;

        Ok(config_dir.join("preferences.toml"))
    }

    fn create_and_get_config_dir() -> Result<PathBuf> {
        Self::project_dirs().and_then(|pd| {
            let config_dir = pd.config_dir();
            Self::create_dirs(&config_dir)?;
            Ok(config_dir.to_path_buf())
        })
    }

    fn project_dirs() -> Result<ProjectDirs> {
        let project_dirs = ProjectDirs::from(
            application_info::QUALIFIER,
            application_info::ORGANISATION,
            application_info::NAME,
        ).ok_or_else(|| io::Error::new(io::ErrorKind::Other, "cannot get the config directory"))?;

        Ok(project_dirs)
    }

    fn create_dirs<P>(path: P) -> Result<()>
    where
        P: AsRef<Path>,
    {
        fs::create_dir_all(path).map_err(Error::from)
    }
}
