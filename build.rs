use std::env;
use std::fs;
use std::io;
use std::path::Path;
use std::process::{ExitStatus, Command};
use std::io::prelude::*;

fn glib_compile_resources(target: &Path, resource: &Path, source_dir: &Option<&Path>) -> io::Result<ExitStatus> {
    let mut command = Command::new("glib-compile-resources");

    command.arg("--target");
    command.arg(target);
    
    if let Some(source_dir) = source_dir {
        command.arg("--sourcedir");
        command.arg(source_dir);
    }

    command.arg(resource);

    command.status()
}

fn generate_rs_from_gresources(output_rs: &Path, input_resource: &Path) -> io::Result<()> {
    let input_file  = fs::File::open(input_resource)?;
    let output_file = fs::File::create(output_rs)?;

    let reader      = io::BufReader::new(&input_file);
    let mut writer  = io::BufWriter::new(&output_file);

    write!(&mut writer, "static RESOURCES_DATA: &'static [u8] = &[")?;
    
    for byte in reader.bytes() {
        write!(&mut writer, "{}, ", byte?)?;
    }

    write!(&mut writer, "];")?;

    Ok(())
}

fn main() -> Result<(), io::Error> {
    let out_dir            = env::var("OUT_DIR").unwrap();
    let out_dir            = Path::new(&out_dir);
    let data_dir           = Path::new("data");

    let input_resource     = data_dir.join("chasopys.gresource.xml");

    let output_resource    = out_dir.join("chasopys.gresource");
    let output_resource_rs = out_dir.join("glib_resource_bundle.rs");

    fs::create_dir_all(out_dir)?;

    glib_compile_resources(&output_resource, &input_resource, &Some(&data_dir))?;
    generate_rs_from_gresources(&output_resource_rs, &output_resource)?;

    Ok(())
}
