# Chasopys

TODO: Delete this and the text above, and describe your app

## Installation


## Usage


## Development

After checking out the repo, do teh BLAH-BLAH-BLAH

macOS:

    brew install gtk+3 adwaita-icon-theme

The application may spit a warning at *stderr*:

    (process:77247): Gtk-WARNING **: 17:35:56.796: Locale not supported by C library.
            Using the fallback 'C' locale.


If this is the case, set `LANG` and `LC_ALL` environment variables to something
meaningful, for example:

    export LC_ALL=en_US.UTF-8
    export LANG=en_US.UTF-8

## Contributing

Bug reports and pull requests are welcome on
[Bitbucket](https://bitbucket.org/manenko/chasopys).  This project is intended
to be a safe, welcoming space for collaboration, and contributors are expected
to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of
conduct.

## License

The application is available as open source under the terms of the [MIT license](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Chasopys project's codebases, issue trackers,
chat rooms and mailing lists is expected to follow the [code of conduct](https://bitbucket.org/manenko/chasopys/src/master/CODE_OF_CONDUCT.md).
